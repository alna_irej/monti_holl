﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Парадокс_Монти_Холла
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int result = 0;
            int iteration = 1;
            for (int i = 0; i < Convert.ToInt32(textBox1.Text); i++)
            {
                //textBox2.Text = (iteration + " из " + textBox1.Text);
                int start = rand.Next(3);
                int step1 = rand.Next(3);
                int step2;
                do
                {
                    step2 = rand.Next(3);
                }
                while (start == step2 || step2 == step1);
                int step3;
                do
                {
                    step3 = rand.Next(3);
                }
                while (step1 == step3 || step2 == step3);
     
                if(step3==start)
                {
                    result++;
                }
                iteration++;
                //Thread.Sleep(1000);
            }
            textBox4.Text = Convert.ToString(result);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int result = 0;
            int iteration = 0;
            for (int i = 0; i < Convert.ToInt32(textBox1.Text); i++)
            {
                //textBox3.Text = (iteration + " из " + textBox1.Text);
                int start = rand.Next(3);
                int step1 = rand.Next(3);
                if (step1 == start)
                {
                    result++;
                }
                iteration++;
                //Thread.Sleep(1000);
            }
            textBox5.Text = Convert.ToString(result);
        }
    }
}
